from datetime import datetime
from src.duration import Duration
from src.summary import Summary


def get_duration(start_time, end_time):
    total_seconds = 0
    TIME_FORMAT = '%I:%M:%S %p'
    join_time = datetime.strptime(start_time.split(', ')[1], TIME_FORMAT)
    leave_time = datetime.strptime(end_time.split(', ')[1], TIME_FORMAT)
    total_seconds += (leave_time - join_time).total_seconds()
    seconds = int(total_seconds) % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    return {
        'hours': hour,
        'minutes': minutes,
        'seconds': seconds
    }


def normalize_summary(raw_data: dict):
    id = raw_data.get('Meeting title') or raw_data.get('Meeting Id')
    title = raw_data.get('Meeting title') or raw_data.get('Meeting Title')
    participants = raw_data.get('Attended participants'
                                ) or raw_data.get('Total Number of Participants')
    start_time = raw_data.get('Start time') or raw_data.get('Meeting Start Time')
    end_time = raw_data.get('End time') or raw_data.get('Meeting End Time')
    duration = get_duration(start_time, end_time)
    normalized_data = {
        'Title': title,
        'Id': id,
        'Attended participants': int(participants),
        'Start Time': start_time,
        'End Time': end_time,
        'Duration': {
            'hours': duration.get('hours'),
            'minutes': duration.get('minutes'),
            'seconds': duration.get('seconds'),
        }
    }
    return normalized_data


def build_summary_object(raw_data: dict):
    duration = Duration(raw_data['Duration']['hours'],
                        raw_data['Duration']['minutes'],
                        raw_data['Duration']['seconds'])
    summary = Summary(raw_data.get('Id'),
                      raw_data.get('Title'),
                      raw_data.get('Attended participants'),
                      raw_data.get('Start Time'),
                      raw_data.get('End Time'),
                      duration)
    return summary
