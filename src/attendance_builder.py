from src.attendance import Attendance
from src.duration import Duration
from src.participant import Participant
from src.summary import Summary


def get_list_of_objects_participants(raw_participants):
    participants = []
    for participant in raw_participants:
        name = participant.get('Full Name')
        join_time = participant.get('Join time')
        leave_time = participant.get('Leave time')
        duration = Duration(*participant['Duration'].values())
        email = participant.get('Email')
        role = participant.get('Role')
        participant_id = participant.get('Participant ID (UPN)') or participant.get('Email')
        new_participant = Participant(name, join_time, leave_time,
                                      duration, email, role, participant_id)
        participants.append(new_participant)
        
    return participants


def build_attendance_object(raw: dict):
    start_datetime = raw.get('Start Time')
    title = raw.get('Title')
    id = raw.get('Id')
    attended_participants = raw.get('Attended participants')
    start_time = raw.get('Start Time')
    end_time = raw.get('End Time')
    duration = Duration(*raw['Duration'].values())
    summary = Summary(id, title, attended_participants, start_time, end_time, duration)
    participants = get_list_of_objects_participants(raw.get('Participants'))
    attendance = Attendance(start_datetime, summary, participants)
    return attendance
