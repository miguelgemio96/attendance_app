from src.duration import Duration
from src.participant import Participant


def get_total_seconds_from_list(time: list) -> int:
    hours, min, sec = 0, 0, 0
    for elem in time:
        if elem.endswith('h'):
            hours = int(elem[:-1])
        if elem.endswith('m'):
            min = int(elem[:-1])
        if elem.endswith('s'):
            sec = int(elem[:-1])
    
    return (hours*3600) + (min * 60) + sec


def get_time_from_seconds(seconds: int) -> str:
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    return f'{hour}h {minutes}m {seconds}s'


def get_total_duration(raws: dict) -> str:
    total_seconds = 0
    for raw in raws:
        time_list = raw['Duration'].split(' ')
        total_seconds += get_total_seconds_from_list(time_list)
        
    str_duration = get_time_from_seconds(total_seconds)
    return str_duration


def resolve_participant_join_last_time(raw: dict):
    name = raw[0].get('Full Name')
    join_time = raw[0].get('Join Time')
    leave_time = raw[-1].get('Leave Time')
    duration = get_total_duration(raw)
    email = raw[0].get('Email')
    role = raw[0].get('Role')
    participant_id = raw[0].get('Participant ID (UPN)')
    resolved_participant = {
        'Name': name,
        'First Join Time': join_time,
        'Last Leave time': leave_time,
        'In-meeting Duration': duration,
        'Email': email,
        'Role': role,
        'Participant ID (UPN)': participant_id
    }
    return resolved_participant


def get_int_hours_min_sec(time: str):
    seconds = get_total_seconds_from_list(time.split(' '))
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    return {
        'hours': hour,
        'minutes': minutes,
        'seconds': seconds
    }


def normalize_participant(raw: dict):
    name = raw.get('Full Name') or raw.get('Name')
    join_time = raw.get('First Join Time') or raw.get('Join Time')
    leave_time = raw.get('Last Leave time') or raw.get('Leave Time')
    duration = get_int_hours_min_sec(raw.get('Duration') or raw.get('In-meeting Duration'))
    email = raw.get('Email')
    role = raw.get('Role')
    participant_id = raw.get('Participant ID (UPN)') or raw.get('Email')
    normalized_participant = {
        'Full Name': name,
        'Join time': join_time,
        'Leave time': leave_time,
        'In-meeting Duration': {
            'Hours': duration.get('hours'),
            'Minutes': duration.get('minutes'),
            'Seconds': duration.get('seconds')
        },
        'Email': email,
        'Role': role,
        'Participant ID (UPN)': participant_id
    }
    return normalized_participant


def build_participant_object(raw: dict):
    if not raw.get('Full Name') or not raw.get('Join time') or not raw.get('Leave time'):
        raise ValueError('Full Name, Join time, Leave time cannot be None')
        
    name = raw.get('Full Name') or raw.get('Name')
    join_time = raw.get('Join time') or raw.get('Join Time')
    leave_time = raw.get('Leave time') or raw.get('Leave Time')
    duration = Duration(*raw['In-meeting Duration'].values())
    email = raw.get('Email')
    role = raw.get('Role')
    participant_id = raw.get('Participant ID (UPN)') or raw.get('Email')
    participant = Participant(name, join_time, leave_time, duration, email, role, participant_id)
    
    return participant
