class Meeting:
    '''Encapsulates a Meeting definition.'''

    def __init__(self, id, title):
        self.attendance = list()
        self.id = id
        self.title = title

    def add_attendance(self, attendance):
        self.attendance += attendance
