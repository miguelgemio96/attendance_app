from src.duration import Duration


class Participant:
    # init method or constructor
    def __init__(self,
                 full_name: str,
                 join_time: str,
                 leave_time: str,
                 in_meeting_duration: Duration,
                 email: str,
                 role: str,
                 id: str
                 ):
        self.full_name = full_name
        self.join_time = join_time
        self.leave_time = leave_time
        self.email = email
        self.role = role
        self.id = id
        self.in_meeting_duration = in_meeting_duration

    @property
    def name(self):
        full_name = self.full_name.split(' ')
        name = full_name[0]
        return name

    @property
    def middle_name(self):
        full_name = self.full_name.split(' ')
        middle_name = full_name[1] if len(full_name) == 4 else None
        return middle_name

    @property
    def first_surname(self):
        full_name = self.full_name.split(' ')
        first_surname = full_name[2] if len(full_name) == 4 else full_name[1]
        return first_surname

    @property
    def second_surname(self):
        full_name = self.full_name.split(' ')
        if len(full_name) == 4:
            return full_name[3]
        
        if len(full_name) == 3:
            return full_name[2]
        
        return None
