from src.duration import Duration


class Summary:
    def __init__(self, id: str, title: str, attended_participants: str, start_time: str,
                 end_time: str, duration: Duration) -> None:
        self.id = id
        self.title = title
        self.attended_participants = attended_participants
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration
